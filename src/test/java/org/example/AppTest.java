package org.example;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest  extends TestCase {

    public AppTest( String testName )
    {
        super( testName );
    }

    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    public void testShouldConvert1() {
        assertEquals("I", App.toRomanNumber(1));
    }

    public void testShouldConvert2() {
        assertEquals("II", App.toRomanNumber(2));
    }

    public void testShouldConvert5()
    {
        assertEquals("V", App.toRomanNumber(5));
    }

    public void testShouldConvert10()
    {
        assertEquals("X", App.toRomanNumber(10));
    }

    public void testShouldConvert20()
    {
        assertEquals("XX", App.toRomanNumber(20));
    }

    public void testShouldConvert50()
    {
        assertEquals("L", App.toRomanNumber(50));
    }

    public void testShouldConvert100()
    {
        assertEquals("C", App.toRomanNumber(100));
    }

    public void testShouldConvert500()
    {
        assertEquals("D", App.toRomanNumber(500));
    }

    public void testShouldConvert1000()
    {
        assertEquals("M", App.toRomanNumber(1000));
    }

    public void testShouldConvert17()
    {
        assertEquals("XVII", App.toRomanNumber(17));
    }

    public void testShouldConvert14()
    {
        assertEquals("XIV", App.toRomanNumber(14));
    }
    public void testShouldConvert400()
    {
        assertEquals("CD", App.toRomanNumber(400));
    }
    public void testShouldConvert90()
    {
        assertEquals("XC", App.toRomanNumber(90));
    }
    public void testShouldConvert40()
    {
        assertEquals("XL", App.toRomanNumber(40));
    }
    public void testShouldConvert9()
    {
        assertEquals("IX", App.toRomanNumber(9));
    }
    public void testShouldConvert4()
    {
        assertEquals("IV", App.toRomanNumber(4));
    }
    public void testShouldConvert444()
    {
        assertEquals("CDXLIV", App.toRomanNumber(444));
    }
    public void testShouldConvert3999()
    {
        assertEquals("MMMCMXCIX", App.toRomanNumber(3999));
    }
}
