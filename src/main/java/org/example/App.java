package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    static String toRomanNumber(Integer arabicNumber) {
        String romanNumber = "";

        while(arabicNumber > 0) {
            String nexRomanChar = getRomanChar(arabicNumber);
            romanNumber += nexRomanChar;
            arabicNumber -= evaluateRomanChar(nexRomanChar);
        }

        return romanNumber;
    }

    private static String getRomanChar(Integer arabicNumber) {
        if (arabicNumber >= 1000) {
            return "M";
        }
        if (arabicNumber >= 900) {
            return "CM";
        }
        if (arabicNumber >= 500) {
            return "D";
        }
        if (arabicNumber >= 400) {
            return "CD";
        }
        if (arabicNumber >= 100) {
            return "C";
        }
        if (arabicNumber >= 90) {
            return "XC";
        }
        if (arabicNumber >= 50) {
            return "L";
        }
        if (arabicNumber >= 40) {
            return "XL";
        }
        if (arabicNumber >= 10) {
            return "X";
        }
        if (arabicNumber >= 9) {
            return "IX";
        }
        if (arabicNumber >= 5) {
            return "V";
        }
        if (arabicNumber >= 4) {
            return "IV";
        }
        return "I";
    }

    private static Integer evaluateRomanChar(String romanChar) {
        switch (romanChar) {
            case "M": {
                return 1000;
            }
            case "CM": {
                return 900;
            }
            case "D": {
                return 500;
            }
            case "CD": {
                return 400;
            }
            case "C": {
                return 100;
            }
            case "XC": {
                return 90;
            }
            case "L": {
                return 50;
            }
            case "XL": {
                return 40;
            }
            case "V": {
                return 5;
            }
            case "IV": {
                return 4;
            }
            case "X": {
                return 10;
            }
            case "IX": {
                return 9;
            }
            case "I": {
                return 1;
            }
        }
        return null;
    }

}
